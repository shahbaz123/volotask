package com.volo.rovervehicle.database.dao


import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.volo.rovervehicle.model.Photo

@Dao
interface PhotoDao {

    @Query("Select * From photo  WHERE type = :type Order By page")
    fun getAllPhotos(type:String): PagingSource<Int, Photo>

    @Query("SELECT * FROM photo")
    fun getAllPhotos(): List<Photo>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPhotos(photo: Photo)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPhotos(photos: List<Photo>)

    @Query("Delete From Photo")
     fun clearAllPhotos()
}