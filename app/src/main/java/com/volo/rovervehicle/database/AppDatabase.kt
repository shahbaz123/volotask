package com.volo.rovervehicle.database


import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.volo.rovervehicle.database.dao.RemoteKeysDao
import com.volo.rovervehicle.database.dao.PhotoDao
import com.volo.rovervehicle.model.Photo
import com.volo.rovervehicle.model.RemoteKeys
import com.volo.rovervehicle.util.Converters

@Database(entities = [Photo::class, RemoteKeys::class], version = 1)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun photoDao(): PhotoDao
    abstract fun getRemoteKeysDao(): RemoteKeysDao
}