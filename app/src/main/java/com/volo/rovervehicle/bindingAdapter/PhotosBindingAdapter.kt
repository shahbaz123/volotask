package com.volo.rovervehicle.bindingAdapter


import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.volo.rovervehicle.adapter.PhotoAdapter
import com.volo.rovervehicle.model.Photo

@BindingAdapter("setPhotos")
fun setPhotos(recyclerView: RecyclerView, photos: List<Photo>?) {
    photos?.let {
        val adapter = recyclerView.adapter as PhotoAdapter
//            adapter.setData(photos)
    }
}

@BindingAdapter("loadImage")
fun loadImage(view: ImageView, url: String?) {
    if (!url.isNullOrEmpty()) {
        Glide.with(view.context).load(url).into(view)
    }
}