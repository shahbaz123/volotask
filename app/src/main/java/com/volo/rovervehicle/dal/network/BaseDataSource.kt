package com.volo.rovervehicle.dal.network


import android.util.Log
import com.volo.rovervehicle.util.AppUtils
import okhttp3.ResponseBody
import retrofit2.Response
import java.util.concurrent.TimeoutException

abstract class BaseDataSource {
    protected suspend fun <T> getResult(
        call: suspend () -> Response<T>
    ): Resource<T> {
        try {
            val response = call()
            /** response handling **/
            if (response.isSuccessful) {
                val body = response.body()
                if (body != null) {
                    return Resource.success(body, "")
                }
            }
            /** handling error **/
            return handleError(response.code(), response.errorBody())
        } catch (e: Exception) {
            /** Handling exception **/
            return when (e) {
                is TimeoutException, is ClassCastException -> error(e.message ?: e.toString())
                else -> throwErrorMessage(
                   "" /*LangUtils.getResourceString(R.string.something_went_wrong_please_try_again)*/
                )
            }
        }
    }

    private fun <T> handleError(responseCode: Int, errorBody: ResponseBody?): Resource<T> {
        return Resource.error("", null, responseCode)
    } // handleError => Method

    /** Fetch Server Error Message and display to user **/

    private fun <T> throwErrorMessage(message: String): Resource<T> {
        if (!AppUtils.isNetworkAvailable()) {
            return Resource.error(""/*LangUtils.getResourceString(R.string.no_internet_connection)*/)
        }
        return Resource.error(message)
    }
}
