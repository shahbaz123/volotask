package com.volo.rovervehicle.dal.network

import android.net.Uri
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.volo.rovervehicle.dal.service.ApiService
import com.volo.rovervehicle.model.Photo


private const val FIRST_PAGE = 1

class PagingSource(
    private val api: ApiService,
    private val type: String
) : PagingSource<Int, Photo>() {

    override fun getRefreshKey(state: PagingState<Int, Photo>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Photo> {
        return try {
            val page = params.key ?: FIRST_PAGE
            val response = api.getPhotos(type, page = page)
            val data: List<Photo> = response.body()?.photos!!
            LoadResult.Page(
                data = data,
                prevKey = if (page == FIRST_PAGE) null else page - 1,
                nextKey = if (response.body()?.photos?.isEmpty()!!) null else page + 1
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }
}