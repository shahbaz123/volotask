package com.volo.rovervehicle.dal.network

data class Resource<out T>(
    val status: Status,
    val data: T?,
    val message: String?,
    val type: Int = -1
) {

    enum class Status {
        SUCCESS, ERROR
    }

    companion object {
        fun <T> success(data: T?, message: String): Resource<T> {
            return Resource(Status.SUCCESS, data, message)
        }

        fun <T> error(message: String, data: T? = null, type: Int = -1): Resource<T> {
            return Resource(Status.ERROR, data, message, type)
        }
    }
}