package com.volo.rovervehicle.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.volo.rovervehicle.adapter.ViewPagerAdapter
import com.google.android.material.tabs.TabLayoutMediator
import com.volo.voloandroidtask.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

val cars = arrayOf(
    "Curiosity",
    "Opportunity",
    "Spirit"
)
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val pagerAdapter by lazy {
        ViewPagerAdapter(supportFragmentManager, lifecycle)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.pager.adapter = pagerAdapter
        TabLayoutMediator(binding.tabLayout, binding.pager) { tab, pos ->
            tab.text= cars[pos]
        }.attach()
    }

}