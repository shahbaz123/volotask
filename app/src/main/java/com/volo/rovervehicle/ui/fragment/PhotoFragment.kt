package com.volo.rovervehicle.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.volo.rovervehicle.adapter.PhotoAdapter
import com.volo.rovervehicle.viewmodel.ViewModel
import com.volo.voloandroidtask.databinding.FragmentPhotosBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

private const val TAG = "PhotoFragment"

@AndroidEntryPoint
class PhotoFragment(val type: String) : Fragment() {

    private lateinit var binding: FragmentPhotosBinding

    private val viewModel: ViewModel by viewModels()

    private val adapter by lazy {
        PhotoAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentPhotosBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.recyclerCars.adapter = adapter

        lifecycleScope.launch {
            viewModel.getPhotos(type).collectLatest { pagingData ->
                adapter.submitData(pagingData)
            }
        }

    }

}