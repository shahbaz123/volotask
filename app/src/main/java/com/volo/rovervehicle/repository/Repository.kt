package com.volo.rovervehicle.repository

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.volo.rovervehicle.dal.network.BaseDataSource
import com.volo.rovervehicle.dal.network.RemoteMediator
import com.volo.rovervehicle.dal.network.PagingSource
import com.volo.rovervehicle.dal.network.Resource
import com.volo.rovervehicle.dal.service.ApiService
import com.volo.rovervehicle.database.AppDatabase
import com.volo.rovervehicle.model.Photo
import com.volo.rovervehicle.model.PhotosResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Repository @Inject constructor(
    private val apiService: ApiService,
    private val db: AppDatabase
) : BaseDataSource() {

    @OptIn(ExperimentalPagingApi::class)
    fun getPhotos(type: String): Flow<PagingData<Photo>> {
        return Pager(
            config = PagingConfig(
                pageSize = 25,
                prefetchDistance = 10,
                initialLoadSize = 25,
            ),
            remoteMediator = RemoteMediator(apiService, db, type)
        ) { db.photoDao().getAllPhotos(type) }.flow
    }


    fun getPhoto(type: String) = Pager(
        pagingSourceFactory = { PagingSource(apiService, type) },
        config = PagingConfig(pageSize = 25)
    ).flow
}