package com.volo.rovervehicle.model


import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import com.volo.rovervehicle.util.Converters
import kotlinx.parcelize.Parcelize
@Entity
@Parcelize
data class Rover(
    @SerializedName("cameras")
    val cameras: List<Camera>,
    @SerializedName("id")
    val roverId: Int,
    @SerializedName("landing_date")
    val landingDate: String,
    @SerializedName("launch_date")
    val launchDate: String,
    @SerializedName("max_date")
    val maxDate: String,
    @SerializedName("max_sol")
    val maxSol: Int,
    @SerializedName("name")
    val roverName: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("total_photos")
    val totalPhotos: Int
):Parcelable