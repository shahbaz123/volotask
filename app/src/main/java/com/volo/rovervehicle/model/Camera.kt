package com.volo.rovervehicle.model


import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
@Entity
@Parcelize
data class Camera(
    @SerializedName("full_name")
    val fullName: String,
    @SerializedName("id")
    val cameraId: Int,
    @SerializedName("name")
    val cameraName: String,
    @SerializedName("rover_id")
    val roverId: Int
):Parcelable