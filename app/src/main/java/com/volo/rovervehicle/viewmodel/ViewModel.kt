package com.volo.rovervehicle.viewmodel


import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.volo.rovervehicle.model.Photo
import com.volo.rovervehicle.repository.Repository

import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@HiltViewModel
class ViewModel @Inject constructor(
    private val repository: Repository
) : ViewModel() {

    fun getPhotos(type: String): Flow<PagingData<Photo>> {
        return repository.getPhotos(type)
            .cachedIn(viewModelScope)
    }

}