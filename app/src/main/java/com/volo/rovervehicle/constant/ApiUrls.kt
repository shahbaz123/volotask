package com.volo.rovervehicle.constant

object ApiUrls {
    /** Api Urls **/
    const val GET_PHOTOS: String = "rovers/{type}/photos"
}