package com.volo.rovervehicle.constant

object Constants {
    /** Timeout **/
    const val REQUEST_TIME_OUT: Long = 120
}