package com.volo.rovervehicle.util

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.volo.rovervehicle.model.Camera

class Converters {
    @TypeConverter
    fun fromGroupTaskMemberList(value: List<Camera>): String {
        val gson = Gson()
        val type = object : TypeToken<List<Camera>>() {}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun toGroupTaskMemberList(value: String): List<Camera> {
        val gson = Gson()
        val type = object : TypeToken<List<Camera>>() {}.type
        return gson.fromJson(value, type)
    }
}