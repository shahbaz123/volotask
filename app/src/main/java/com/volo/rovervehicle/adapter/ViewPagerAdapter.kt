package com.volo.rovervehicle.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.volo.rovervehicle.ui.fragment.PhotoFragment

private const val NUM_TABS = 3

class ViewPagerAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) :
    FragmentStateAdapter(fragmentManager, lifecycle) {

    override fun getItemCount(): Int {
        return NUM_TABS
    }

    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> return PhotoFragment("Curiosity")
            1 -> return PhotoFragment("Opportunity")
            2 -> return PhotoFragment("Spirit")
        }
        return PhotoFragment("Curiosity")
    }
}