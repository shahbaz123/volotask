package com.volo.rovervehicle
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingConfig
import androidx.paging.PagingState
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.volo.rovervehicle.dal.service.ApiService
import com.volo.rovervehicle.database.AppDatabase
import com.volo.rovervehicle.model.Photo
import com.volo.rovervehicle.model.RemoteKeys
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalPagingApi
@RunWith(AndroidJUnit4::class)
class RemoteMediatorTest {

    private lateinit var apiService: ApiService
    private lateinit var appDatabase: AppDatabase
    private lateinit var remoteMediator: com.volo.rovervehicle.dal.network.RemoteMediator

    @Before
    fun setup() {
        apiService = MockApiService()
        appDatabase = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java
        ).allowMainThreadQueries().build()

        remoteMediator =
            com.volo.rovervehicle.dal.network.RemoteMediator(apiService, appDatabase, "test")
    }

    @After
    fun tearDown() {
        appDatabase.close()
    }

    @Test
    fun testLoadFirstPage() = runBlocking {
        // Mock API response
        val photos = listOf(
            Photo(id = 1, imgSrc = "https://example.com/photo1.jpg"),
            Photo(id = 2, imgSrc = "https://example.com/photo2.jpg")
        )
        (apiService as MockApiService).setPhotos(photos)

        // Call the load function
        val loadType = LoadType.REFRESH
        val pagingState = PagingState<Int, Photo>(
            listOf(), null, PagingConfig(10), 0
        )
        val result = remoteMediator.load(loadType, pagingState)

        // Assertions
        assert(result is androidx.paging.RemoteMediator.MediatorResult.Success)
        val successResult = result as androidx.paging.RemoteMediator.MediatorResult.Success
        assert(successResult.endOfPaginationReached == false)

        // Verify database operations
        val photoDao = appDatabase.photoDao()
        val remoteKeysDao = appDatabase.getRemoteKeysDao()
        assert(photoDao.getAllPhotos().size == 2)
        assert(remoteKeysDao.getAllRemoteKeys().size == 2)
    }

    @Test
    fun testLoadPageAfterFirstPage() = runBlocking {
        // Mock API response
        val photos = listOf(
            Photo(id = 3, imgSrc = "https://example.com/photo3.jpg"),
            Photo(id = 4, imgSrc = "https://example.com/photo4.jpg")
        )
        (apiService as MockApiService).setPhotos(photos)

        // Set up existing remote keys
        val remoteKeysDao = appDatabase.getRemoteKeysDao()
        remoteKeysDao.insert(
            RemoteKeys(photoID = 2, prevKey = 1, currentPage = 1, nextKey = null, type = "test")
        )

        // Call the load function
        val loadType = LoadType.APPEND
        val pagingState = PagingState<Int, Photo>(
            listOf(), null, PagingConfig(10), 1
        )
        val result = remoteMediator.load(loadType, pagingState)

        // Assertions
        assert(result is androidx.paging.RemoteMediator.MediatorResult.Success)
        val successResult = result as androidx.paging.RemoteMediator.MediatorResult.Success
        assert(successResult.endOfPaginationReached == false)

        // Verify database operations
        val photoDao = appDatabase.photoDao()
        assert(photoDao.getAllPhotos().size == 4)
        assert(remoteKeysDao.getAllRemoteKeys().size == 4)
    }
}