package com.volo.rovervehicle

import com.volo.rovervehicle.dal.service.ApiService
import com.volo.rovervehicle.model.Photo
import com.volo.rovervehicle.model.PhotosResponse
import retrofit2.Response

class MockApiService: ApiService {
    private var photosResponse: Response<PhotosResponse>? = null

    fun setPhotos(photos: List<Photo>) {
        photosResponse = Response.success(PhotosResponse(photos))
    }

    override suspend fun getPhotos(
        type: String,
        apiKey: String,
        sol: Int,
        page: Int
    ): Response<PhotosResponse> {
        return photosResponse ?: Response.error(500, null)
    }
}
